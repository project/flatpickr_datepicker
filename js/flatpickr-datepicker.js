/**
 * @file
 */
 (function($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.multiple_dates = {
    attach: function(context, settings) {
      var dateFormat = drupalSettings.settings_data.dateFormat;
      var mode = drupalSettings.settings_data.mode;
      var disable = drupalSettings.settings_data.disable;
      var disabled_weeks = drupalSettings.settings_data.disabled_weeks;
      var minDate = drupalSettings.settings_data.minDate;
      var maxDate = drupalSettings.settings_data.maxDate;
      var enableTime = drupalSettings.settings_data.enableTime;
      var settings_data = drupalSettings.settings_data;
      jQuery.each(settings_data, function(index, item) {
        var $this = $('.' + index + '-flatpickr');
        var disabledData = [];
        var i = 0;
        if (typeof item.disable != 'undefined') {
          if (item.disable.length > 0) {
            var item_data = $.each(item.disable, function(indexes, values) {
              if (typeof values == 'object') {
                $.each(values, function(key, value) {
                  disabledData.push(value);
                });
              } else {
                disabledData.push(values);
              }
            });
          }
        }
        if (typeof item.disabled_weeks != 'undefined') {
          var disabledWeeks = item.disabled_weeks;
          if (disabledWeeks.length > 0) {
            var disabledDatesWeek = function(date) {
              if (jQuery.inArray(date.getDay(), disabledWeeks) !== -1) {
                return true;
              }
            };
            disabledData.push(disabledDatesWeek);
          }
        }
        var enabledData = [];
        var i = 0;
        if (typeof item.enable != 'undefined') {
          if (item.enable.length > 0) {
            var item_data = $.each(item.enable, function(indexes, values) {
              if (typeof values == 'object') {
                $.each(values, function(key, value) {
                  enabledData.push(value);
                });
              } else {
                enabledData.push(values);
              }
            });
          }
        }
        $this.flatpickr({
          monthSelectorType: 'dropdown',
          enableTime: true,
          allowInput: true,
          dateFormat: item.dateFormat,
          mode: item.mode,
          defaultDate: item.defaultDate,
          disable: disabledData,
          enable1: enabledData,
          enable: enabledData,
          minDate: item.minDate,
          maxDate: item.maxDate,
        });
      });
    }
  };
})(jQuery, Drupal, drupalSettings);