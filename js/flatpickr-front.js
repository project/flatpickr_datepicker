/**
 * @file
 */
 (function($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.multiple_dates = {
    attach: function(context, settings) {
      var $this = $('.flatpickr-datepicker-calendar');
      $this.each(function(i, obj) {
        var defaultDate = $(obj).attr('data-dates');
        var mode = $(obj).attr('data-type');
        obj.flatpickr({
          inline: true,
          mode: mode,
          defaultDate: defaultDate,
        });
      });
    }
  };
})(jQuery, Drupal, drupalSettings);