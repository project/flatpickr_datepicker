# Flatpickr Datepicker

- Flatpickr Datepicker module provides a custom field for flatpickr datepicker and the widget which provides form element using the jQuery UI datetimepicker. This module that enables jQuery UI calendar to manage flatpickr datepicker with the following features:
    
	- Select date ranges.
	- Pick flatpickr datepicker not in secuence.
	- Define a maximum number of pickable dates.
	- Define a range X days from where it is possible to select Y dates.
	- Define unavailable dates.


## Contents of this file

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

- jquery_ui_datepicker


## Installation

- To install, copy the Flatpickr Datepicker directory and
  all its contents to your modules directory.

- To enable the module go to Administer > Modules, and enable
  "Flatpickr Datepicker".


## Configuration

- Create a field for flatpickr datepicker. Manage fields > Add a new field > Flatpickr Datepicker

- Create a widget for Flatpickr Datepicker field in a content type.

- Edit the Content type and navigate to "Manage form display" tab.

- Under "Widget" select widget for Flatpickr Datepicker for "Flatpickr Datepicker".

	- Datepicker type
	- Set maximum picks
	- Days range
	- Disable specific dates from calendar
	- Min date
	- Max date
	- Disable specific days in week
	- Number Of Months

- Edit the Content type and navigate to "Manage display" tab.

- Under "FORMAT" select widget for Flatpickr Datepicker for "Flatpickr Datepicker".

	- Months display layout
	- Date format
	- Number of months


## Maintainers
- Sujan Shrestha - [sujan.shrestha](https://www.drupal.org/u/sujanshrestha)
