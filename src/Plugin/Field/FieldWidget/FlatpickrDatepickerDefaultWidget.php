<?php

namespace Drupal\flatpickr_datepicker\Plugin\Field\FieldWidget;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'flatpickr_datepicker_default_widget' widget.
 *
 * @FieldWidget(
 *   id = "flatpickr_datepicker_default_widget",
 *   label = @Translation("FlatpickrDatepicker Widget"),
 *   field_types = {
 *     "flatpickr_datepicker"
 *   }
 * )
 */

class FlatpickrDatepickerDefaultWidget extends WidgetBase implements WidgetInterface
{
    /**
     * Returns the form for a single field widget.
     *
     * Field widget form elements should be based on the passed-in $element, which
     * contains the base form element properties derived from the field
     * configuration.
     *
     * The BaseWidget methods will set the weight, field name and delta values for
     * each form element. If there are multiple values for this field, the
     * formElement() method will be called as many times as needed.
     *
     * Other modules may alter the form element provided by this function using
     * hook_field_widget_form_alter() or
     * hook_field_widget_WIDGET_TYPE_form_alter().
     *
     * The FAPI element callbacks (such as #process, #element_validate,
     * #value_callback, etc.) used by the widget do not have access to the
     * original $field_definition passed to the widget's constructor. Therefore,
     * if any information is needed from that definition by those callbacks, the
     * widget implementing this method, or a
     * hook_field_widget[_WIDGET_TYPE]_form_alter() implementation, must extract
     * the needed properties from the field definition and set them as ad-hoc
     * $element['#custom'] properties, for later use by its element callbacks.
     *
     * @param \Drupal\Core\Field\FieldItemListInterface $items
     *   Array of default values for this field.
     * @param int $delta
     *   The order of this item in the array of sub-elements (0, 1, 2, etc.).
     * @param array $element
     *   A form element array containing basic properties for the widget:
     *   - #field_parents: The 'parents' space for the field in the form. Most
     *       widgets can simply overlook this property. This identifies the
     *       location where the field values are placed within
     *       $form_state->getValues(), and is used to access processing
     *       information for the field through the getWidgetState() and
     *       setWidgetState() methods.
     *   - #title: The sanitized element label for the field, ready for output.
     *   - #description: The sanitized element description for the field, ready
     *     for output.
     *   - #required: A Boolean indicating whether the element value is required;
     *     for required multiple value fields, only the first widget's values are
     *     required.
     *   - #delta: The order of this item in the array of sub-elements; see $delta
     *     above.
     * @param array $form
     *   The form structure where widgets are being attached to. This might be a
     *   full form structure, or a sub-element of a larger form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The form elements for a single widget for this field.
     *
     * @see hook_field_widget_form_alter()
     * @see hook_field_widget_WIDGET_TYPE_form_alter()
     */
    public function formElement(
        FieldItemListInterface $items,
        $delta,
        array $element,
        array &$form,
        FormStateInterface $form_state
    ) {
        $settings = [];

        $field_name = $items->getFieldDefinition()->getName();

        foreach ($element as $key => $value) {
            $element["flatpickr_datepicker"][$key] = $value;
        }

        $get_value = $items->getValue();
        $defaultDate = "";

        if (
            is_array($get_value) &&
            isset($get_value[0]["flatpickr_datepicker"])
        ) {
            $defaultDate = $get_value[0]["flatpickr_datepicker"];
        }

        $date_format = $this->getSetting("date_format");
        $datetime_format = $this->getSetting("datetime_format");
        $datepicker_type = $this->getSetting("datepicker_type");
        $enable_only_specific_dates = $this->getSetting(
            "enable_only_specific_dates"
        );

        $enable_validation =
        $this->getSetting("enable_validation") &&
        $this->getSetting("enable_validation") == 1
        ? true
        : false;

        $settings[$field_name]["dateFormat"] = $datetime_format;
        $settings[$field_name]["disable"] = [];
        $settings[$field_name]["enable"] = [];
        if ($datepicker_type == "date_range") {
            $settings[$field_name]["mode"] = "range";
            $defaultDate = array_map("trim", explode("to", $defaultDate));
            if (!isset($defaultDate[1])) {
                $defaultDate = [];
            }
        } elseif ($datepicker_type == "multiple_dates") {
            $settings[$field_name]["mode"] = "multiple";
            $defaultDate = array_map("trim", explode(",", $defaultDate));
        } else {
            $settings[$field_name]["mode"] = "single";
            $defaultDate = array_map("trim", explode(",", $defaultDate));
        }

        $settings[$field_name]["defaultDate"] = $defaultDate;

        if ($enable_only_specific_dates) {
            $enabling_specific_dates = $this->getSetting(
                "enabling_specific_dates"
            );
            $enabling_ranges_date = $this->getSetting("enabling_ranges_date");

            if ($enabling_specific_dates != "") {
                $enabling_specific_dates = array_map(
                    "trim",
                    explode(",", $enabling_specific_dates)
                );
                if (is_array($enabling_specific_dates)) {
                    $settings[$field_name]["enable"] = $enabling_specific_dates;
                }
            }
            if ($enabling_ranges_date != "") {
                $enabling_ranges_date_arr = array_map(
                    "trim",
                    explode(",", $enabling_ranges_date)
                );
                $enabling_ranges_dates = [];
                if (is_array($enabling_ranges_date_arr)) {
                    foreach (
                        $enabling_ranges_date_arr
                        as $key => $range_date_arr
                    ) {
                        $range_dates = array_map(
                            "trim",
                            explode("to", $range_date_arr)
                        );
                        if (is_array($range_dates) && isset($range_dates[1])) {
                            $from = $range_dates[0];
                            $to = $range_dates[1];
                            $enabling_ranges_dates[] = [
                                "from" => $from,
                                "to" => $to,
                            ];
                        }
                    }
                }
                $settings[$field_name]["enable"][] = $enabling_ranges_dates;
            }
        } else {
            $disabling_specific_dates = $this->getSetting(
                "disabling_specific_dates"
            );
            $min_date = $this->getSetting("min_date");
            $max_date = $this->getSetting("max_date");
            $disable_days_week = $this->getSetting("disable_days_week");
            $disabling_ranges_date = $this->getSetting("disabling_ranges_date");

            if ($disabling_specific_dates != "") {
                $disabling_specific_dates = array_map(
                    "trim",
                    explode(",", $disabling_specific_dates)
                );

                if (is_array($disabling_specific_dates)) {
                    $settings[$field_name][
                        "disable"
                    ] = $disabling_specific_dates;
                }
            }
            if ($disabling_ranges_date != "") {
                $disabling_ranges_date_arr = array_map(
                    "trim",
                    explode(",", $disabling_ranges_date)
                );
                $disabling_ranges_dates = [];
                foreach (
                    $disabling_ranges_date_arr
                    as $key => $range_date_arr
                ) {
                    $range_dates = array_map(
                        "trim",
                        explode("to", $range_date_arr)
                    );
                    if (isset($range_dates[1])) {
                        $from = $range_dates[0];
                        $to = $range_dates[1];
                        $disabling_ranges_dates[] = [
                            "from" => $from,
                            "to" => $to,
                        ];
                    }
                }
                $settings[$field_name]["disable"][] = $disabling_ranges_dates;
            }

            if (is_array($disable_days_week) && !empty($disable_days_week)) {
                $disabled_weeks = [];
                foreach ($disable_days_week as $key => $week) {
                    if ($week != "0") {
                        $disabled_weeks[] = $week - 1;
                    }
                }

                $settings[$field_name]["disabled_weeks"] = $disabled_weeks;
            }

            if ($min_date != "") {
                $settings[$field_name]["minDate"] = $min_date;
            }
            if ($max_date != "") {
                $settings[$field_name]["maxDate"] = $max_date;
            }
        }

        $enable_validation = $this->getSetting("enable_validation");
        if ($enable_validation) {
            $settings[$field_name]["enableTime"] = $enable_validation;
        }

        $element["flatpickr_datepicker"]["#type"] = "textfield";
        $element["flatpickr_datepicker"]["#default_value"] = isset(
            $items[$delta]->flatpickr_datepicker
        )
        ? $items[$delta]->flatpickr_datepicker
        : null;

        $element["flatpickr_datepicker"]["#attached"]["drupalSettings"][
            "settings_data"
        ] = $settings;
        $element["flatpickr_datepicker"]["#attached"]["library"][] =
        "flatpickr_datepicker/flatpickr_datepicker";
        $element["flatpickr_datepicker"]["#attributes"]["class"][] =
        $field_name . "-flatpickr";

        $element["flatpickr_datepicker"]["#element_validate"] = [
            [$this, "validate"],
        ];

        return $element;
    }

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
            "date_format" => "Y-m-d",
            "datetime_format" => "Y-m-d H:i A",
            "datepicker_type" => "",
            "enable_only_specific_dates" => "",
            "disabling_specific_dates" => "",
            "enabling_specific_dates" => "",
            "enabling_ranges_date" => "",
            "disabling_ranges_date" => "",
            "min_date" => "",
            "max_date" => "",
            "disable_days_week" => "",
            "enable_validation" => true,
        ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = [];

        $elements["date_format"] = [
            "#type" => "select",
            "#title" => $this->t("Custom date format"),
            "#description" => $this->t("Custom date format"),
            "#options" => [
                "Y-m-d" => $this->t("Y-m-d"),
            ],
            "#default_value" => $this->getSetting("date_format") ?? "Y-m-d",
            "#required" => true,
            "#access" => false,
        ];

        $elements["datetime_format"] = [
            "#type" => "select",
            "#title" => $this->t("Custom date format with time"),
            "#description" => $this->t("Custom date format"),
            "#options" => [
                "Y-m-d H:i A" => $this->t("Y-m-d H:i A"),
            ],
            "#default_value" =>
            $this->getSetting("datetime_format") ?? "Y-m-d H:i A",
            "#required" => true,
            "#access" => false,
        ];

        $elements["datepicker_type"] = [
            "#type" => "select",
            "#title" => $this->t("Datepicker type"),
            "#description" => $this->t("Datepicker type"),
            "#options" => [
                "single_datetime" => $this->t("Single datetime"),
                "date_range" => $this->t("Date range"),
                "multiple_dates" => $this->t("Multiple dates"),
            ],
            "#default_value" =>
            $this->getSetting("datepicker_type") ?? "multiple_dates",
            "#required" => false,
            "#wrapper_attributes" => [
                "class" => ["datepicker-type"],
            ],
        ];

        $sample_date = date("Y-m-d");
        $enable_only_specific_dates =
        $this->getSetting("enable_only_specific_dates") ?? 0;
        $elements["enable_only_specific_dates"] = [
            "#type" => "checkbox",
            "#title" => $this->t("Enable only specific dates"),
            "#description" => $this->t(
                "Enable only specific dates for selection"
            ),
            "#default_value" => $enable_only_specific_dates,
            "#required" => false,
            "#wrapper_attributes" => [
                "class" => ["enable-only-specific-dates"],
            ],
        ];

        $elements["enabling_specific_dates"] = [
            "#type" => "textarea",
            "#title" => $this->t("Enabled specific dates"),
            "#description" => $this->t(
                "Enable only specific dates from calendar If you’d like to make only certain dates available for selection then enter the days in following format Y-m-d e.g. " .
                $sample_date .
                " separated by comma."
            ),
            "#default_value" => $this->getSetting("enabling_specific_dates"),
            "#required" => false,
            "#wrapper_attributes" => [
                "class" => [
                    "enabling-specific-dates enabled-only-specific-dates enabled-specific-dates",
                ],
            ],
            "#states" => [
                "visible" => [
                    ':input[name$="[settings_edit_form][settings][enable_only_specific_dates]"]' => [
                        "checked" => true,
                    ],
                ],
            ],
        ];

        $current_year = date("Y");

        $elements["enabling_ranges_date"] = [
            "#type" => "textarea",
            "#title" => $this->t("Enabling range(s) of dates"),
            "#description" => $this->t(
                "If you’d like to make only certain date ranges available for selection then enter the date range in following format Y-m-d to Y-m-d e.g. " .
                $current_year .
                "-02-05 to " .
                $current_year .
                "-02-21, " .
                $current_year .
                "-08-10 to " .
                $current_year .
                "-10-05 etc separated by comma."
            ),
            "#default_value" => $this->getSetting("enabling_ranges_date"),
            "#required" => false,
            "#wrapper_attributes" => [
                "class" => [
                    "enabling-ranges-date enabled-only-specific-dates enabled-" .
                    $current_year .
                    "-02-05 to " .
                    $current_year .
                    "-02-21, " .
                    $current_year .
                    "-08-10 to " .
                    $current_year .
                    "-10-05 etcrange-dates",
                ],
            ],
            "#states" => [
                "visible" => [
                    ':input[name$="[settings_edit_form][settings][enable_only_specific_dates]"]' => [
                        "checked" => true,
                    ],
                ],
            ],
        ];

        $elements["disabling_specific_dates"] = [
            "#type" => "textarea",
            "#title" => $this->t("Disabling specific dates"),
            "#description" => $this->t(
                "If you’d like to make certain dates unavailable for selection then enter the days in following format Y-m-d e.g. " .
                $sample_date .
                " separated by comma."
            ),
            "#default_value" => $this->getSetting("disabling_specific_dates"),
            "#required" => false,
            "#wrapper_attributes" => [
                "class" => [
                    "disabling-specific-dates disabled-only-specific-dates disabled-specific-dates",
                ],
            ],
            "#states" => [
                "visible" => [
                    ':input[name$="[settings_edit_form][settings][enable_only_specific_dates]"]' => [
                        "!checked" => true,
                    ],
                ],
            ],
        ];

        $elements["disabling_ranges_date"] = [
            "#type" => "textarea",
            "#title" => $this->t("Disabling range(s) of dates"),
            "#description" => $this->t(
                "If you’d like to make only certain date ranges unavailable for selection then enter the date range in following format Y-m-d to Y-m-d e.g. " .
                $current_year .
                "-02-05 to " .
                $current_year .
                "-02-21, " .
                $current_year .
                "-08-10 to " .
                $current_year .
                "-10-05 etc separated by comma."
            ),
            "#default_value" => $this->getSetting("disabling_ranges_date"),
            "#required" => false,
            "#wrapper_attributes" => [
                "class" => [
                    "disabling-ranges-date disabled-only-specific-dates disabled-range-dates",
                ],
            ],
            "#states" => [
                "visible" => [
                    ':input[name$="[settings_edit_form][settings][enable_only_specific_dates]"]' => [
                        "!checked" => true,
                    ],
                ],
            ],
        ];

        $elements["min_date"] = [
            "#type" => "textfield",
            "#title" => $this->t("Min date"),
            "#description" => $this->t(
                'Min date option specifies the minimum/earliest date (inclusively) allowed for selection. Eg: "' .
                $current_year .
                '-05-15", "today" etc'
            ),
            "#default_value" => $this->getSetting("min_date"),
            "#required" => false,
            "#wrapper_attributes" => [
                "class" => ["min-date disabled-only-specific-dates"],
            ],
            "#states" => [
                "visible" => [
                    ':input[name$="[settings_edit_form][settings][enable_only_specific_dates]"]' => [
                        "!checked" => true,
                    ],
                ],
            ],
        ];

        if ($enable_only_specific_dates) {
            $elements["min_date"]["#wrapper_attributes"]["class"][] = "hidden1";
        }

        $elements["max_date"] = [
            "#type" => "textfield",
            "#title" => $this->t("Max date"),
            "#description" => $this->t(
                'Max date option specifies the maximum/latest date (inclusively) allowed for selection. Eg: "' .
                $current_year .
                '-12-05", "14 days from now" etc'
            ),
            "#default_value" => $this->getSetting("max_date"),
            "#required" => false,
            "#wrapper_attributes" => [
                "class" => [
                    "max-date disabled-only-specific-dates disabled-only-specific-dates",
                ],
            ],
            "#states" => [
                "visible" => [
                    ':input[name$="[settings_edit_form][settings][enable_only_specific_dates]"]' => [
                        "!checked" => true,
                    ],
                ],
            ],
        ];

        if ($enable_only_specific_dates) {
            $elements["max_date"]["#wrapper_attributes"]["class"][] = "hidden1";
        }
        $elements["disable_days_week"] = [
            "#type" => "checkboxes",
            "#title" => $this->t("Disable specific days in week"),
            "#description" => $this->t(
                "Select days which are disabled in calendar, etc. weekends or just Friday"
            ),
            "#options" => [
                "1" => $this->t("Sunday"),
                "2" => $this->t("Monday"),
                "3" => $this->t("Tuesday"),
                "4" => $this->t("Wednesday"),
                "5" => $this->t("Thursday"),
                "6" => $this->t("Friday"),
                "7" => $this->t("Saturday"),
            ],
            "#default_value" => $this->getSetting("disable_days_week"),
            "#required" => false,
            "#attributes" => [
                "class" => ["disable-days-week disabled-only-specific-dates"],
            ],
            "#states" => [
                "visible" => [
                    ':input[name$="[settings_edit_form][settings][enable_only_specific_dates]"]' => [
                        "!checked" => true,
                    ],
                ],
            ],
        ];

        if ($enable_only_specific_dates) {
            $elements["disable_days_week"]["#attributes"]["class"][] =
            "hidden1";
        }
        $elements["enable_validation"] = [
            "#type" => "checkbox",
            "#title" => $this->t("Enable validation"),
            "#description" => $this->t("Enable validation on the datepicker to prevent users to enter the disabled dates."),
            "#default_value" =>
            $this->getSetting("enable_validation") ?? "true",
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = [];

        $date_format = $this->getSetting("date_format");
        $datetime_format = $this->getSetting("datetime_format");
        $datepicker_type = $this->getSetting("datepicker_type");
        $enable_only_specific_dates = $this->getSetting(
            "enable_only_specific_dates"
        )
        ? "true"
        : "false";
        $disabling_specific_dates = $this->getSetting(
            "disabling_specific_dates"
        );
        $enabling_specific_dates = $this->getSetting("enabling_specific_dates");
        $enabling_ranges_date = $this->getSetting("enabling_ranges_date");
        $disabling_ranges_date = $this->getSetting("disabling_ranges_date");
        $min_date = $this->getSetting("min_date");
        $max_date = $this->getSetting("max_date");
        $disable_days_week = $this->getSetting("disable_days_week");
        $enable_validation = $this->getSetting("enable_validation")
        ? "true"
        : "false";

        $disabled_days = [];
        if (is_array($disable_days_week)) {
            $days = [
                1 => "Sunday",
                2 => "Monday",
                3 => "Tuesday",
                4 => "Wednesday",
                5 => "Thursday",
                6 => "Friday",
                7 => "Saturday",
            ];

            foreach ($disable_days_week as $key => $day_num) {
                if ($day_num != 0) {
                    $disabled_days[] = $days[$day_num];
                }
            }
        }

        $disabled_days_list = implode(", ", $disabled_days);

        $summary_content = "Date format: " . $datetime_format . "<br>";
        $summary_content .= "Datepicker type: " . $datepicker_type . "<br>";
        $summary_content .=
        "Enable only specific dates: " .
        $enable_only_specific_dates .
        "<br>";
        $summary_content .=
        "Excluded date(s): " .
        flatpickr_limit_characters($disabling_specific_dates, "26") .
        "<br>";
        $summary_content .=
        "Enabled date(s): " .
        flatpickr_limit_characters($enabling_specific_dates, "26") .
        "<br>";
        $summary_content .=
        "Enabled range date(s): " .
        flatpickr_limit_characters($enabling_ranges_date, "26") .
        "<br>";
        $summary_content .=
        "Disabled range date(s): " .
        flatpickr_limit_characters($disabling_ranges_date, "26") .
        "<br>";
        $summary_content .= "Min date: " . $min_date . "<br>";
        $summary_content .= "Max date: " . $max_date . "<br>";
        $summary_content .=
        "Disable days week: " . $disabled_days_list . "<br>";
        $summary_content .= "Enable validation: " . $enable_validation . "<br>";

        // Implement settings summary.
        $summary[] = $this->t($summary_content);

        return $summary;
    }

    /**
     * Validate flatpickr datepicker field
     */
    public function validate($element, FormStateInterface $form_state)
    {
        $enable_validation =
        $this->getSetting("enable_validation") &&
        $this->getSetting("enable_validation") == 1
        ? true
        : false;

        if ($enable_validation) {
            $selected_dates = $element["#value"];

            if ($selected_dates != "") {
                $date_format = $this->getSetting("date_format");
                $datetime_format = $this->getSetting("datetime_format");
                $datepicker_type = $this->getSetting("datepicker_type");
                $enable_only_specific_dates = $this->getSetting(
                    "enable_only_specific_dates"
                );
                $disabling_specific_dates = $this->getSetting(
                    "disabling_specific_dates"
                );
                $enabling_specific_dates = $this->getSetting(
                    "enabling_specific_dates"
                );
                $enabling_ranges_date = $this->getSetting(
                    "enabling_ranges_date"
                );
                $disabling_ranges_date = $this->getSetting(
                    "disabling_ranges_date"
                );
                $min_date = $this->getSetting("min_date");
                $max_date = $this->getSetting("max_date");
                $disable_days_week = $this->getSetting("disable_days_week");
                $enable_validation =
                $this->getSetting("enable_validation") &&
                $this->getSetting("enable_validation") == 1
                ? true
                : false;
                $error_message = "";
                $selected_dates_arr = array_map(
                    "trim",
                    explode(",", $selected_dates)
                );
                $start_date = $selected_dates_arr[0];
                $end_date = end($selected_dates_arr);

                // check if Enable only specific dates
                if ($enable_only_specific_dates) {
                    $valid = true;
                    switch ($datepicker_type) {
                        case "single_datetime":
                        $selected_dates_arr = array_map(
                            "trim",
                            explode(",", $selected_dates)
                        );
                        if (isset($selected_dates_arr[0])) {
                            $selected_date = $selected_dates_arr[0];

                                // check if selected date belogs to the enabled dates
                            if ($enabling_specific_dates != "") {
                                $enabling_specific_dates_arr = array_map(
                                    "trim",
                                    explode(",", $enabling_specific_dates)
                                );
                                $enabling_specific_dates_arr = reformat_dates(
                                    $enabling_specific_dates_arr
                                );
                                $selected_date = reformat_date(
                                    $selected_date
                                );
                                if (
                                    !in_array(
                                        $selected_date,
                                        $enabling_specific_dates_arr
                                    )
                                ) {
                                    $valid = false;
                                }
                            }

                            if (!$valid) {
                                $valid = true;

                                    // check if selected date belogs to the enabled dates
                                if ($enabling_ranges_date != "") {
                                    $enabling_ranges_date_arr = array_map(
                                        "trim",
                                        explode(",", $enabling_ranges_date)
                                    );
                                    if (
                                        is_array($enabling_ranges_date_arr)
                                    ) {
                                        foreach (
                                            $enabling_ranges_date_arr
                                            as $disabled_range_key =>
                                            $disabled_range_value
                                        ) {
                                            $enabling_ranges_date_from_to = array_map(
                                                "trim",
                                                explode(
                                                    "to",
                                                    $disabled_range_value
                                                )
                                            );
                                            if (
                                                is_array(
                                                    $enabling_ranges_date_from_to
                                                ) &&
                                                isset(
                                                    $enabling_ranges_date_from_to[1]
                                                )
                                            ) {
                                                $enabling_date_from =
                                                $enabling_ranges_date_from_to[0];
                                                $enabling_date_to =
                                                $enabling_ranges_date_from_to[1];
                                                $flatpickr_is_date_bwtween = flatpickr_is_date_bwtween(
                                                    $selected_date,
                                                    $enabling_date_from,
                                                    $enabling_date_to
                                                );
                                                if (
                                                    !$flatpickr_is_date_bwtween
                                                ) {
                                                    $valid = false;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        break;

                        case "date_range":
                        $valid = true;

                        $selected_dates_arr = array_map(
                            "trim",
                            explode("to", $selected_dates)
                        );

                        if (isset($selected_dates_arr[1])) {
                            $selected_dates_arr_start =
                            $selected_dates_arr[0];
                            $selected_dates_arr_end =
                            $selected_dates_arr[1];

                            if ($enabling_specific_dates != "") {
                                $enabling_specific_dates_arr = array_map(
                                    "trim",
                                    explode(",", $enabling_specific_dates)
                                );

                                $selected_dates_arr_start_donly = explode(
                                    " ",
                                    $selected_dates_arr_start
                                );
                                $selected_dates_start_donly =
                                $selected_dates_arr_start_donly[0];

                                $selected_dates_arr_end_donly = explode(
                                    " ",
                                    $selected_dates_arr_end
                                );
                                $selected_dates_end_donly =
                                $selected_dates_arr_end_donly[0];

                                    // check if selected date belogs to the enabled dates

                                $enabling_specific_dates_arr = reformat_dates(
                                    $enabling_specific_dates_arr
                                );
                                $selected_dates_start_donly = reformat_date(
                                    $selected_dates_start_donly
                                );
                                $selected_dates_end_donly = reformat_date(
                                    $selected_dates_end_donly
                                );
                                if (
                                    !in_array(
                                        $selected_dates_start_donly,
                                        $enabling_specific_dates_arr
                                    ) ||
                                    !in_array(
                                        $selected_dates_end_donly,
                                        $enabling_specific_dates_arr
                                    )
                                ) {
                                    $valid = false;
                                }
                            }

                            if (!$valid) {
                                $valid = true;
                                    // check if selected date belogs to the enabled dates

                                if ($enabling_ranges_date != "") {
                                    $enabling_ranges_date_arr = array_map(
                                        "trim",
                                        explode(",", $enabling_ranges_date)
                                    );

                                    if (
                                        is_array($enabling_ranges_date_arr)
                                    ) {
                                        foreach (
                                            $enabling_ranges_date_arr
                                            as $disabled_range_key =>
                                            $enabled_range_value
                                        ) {
                                            $enabling_ranges_date_from_to = array_map(
                                                "trim",
                                                explode(
                                                    "to",
                                                    $enabled_range_value
                                                )
                                            );

                                            if (
                                                is_array(
                                                    $enabling_ranges_date_from_to
                                                ) &&
                                                isset(
                                                    $enabling_ranges_date_from_to[1]
                                                )
                                            ) {
                                                $enabling_date_from =
                                                $enabling_ranges_date_from_to[0];
                                                $enabling_date_to =
                                                $enabling_ranges_date_from_to[1];

                                                $enabling_date_from_str = strtotime(
                                                    $enabling_date_from
                                                );
                                                $enabling_date_to_str = strtotime(
                                                    $enabling_date_to
                                                );

                                                $selected_dates_arr_start_str = strtotime(
                                                    "12:00 a.m.",
                                                    strtotime(
                                                        $selected_dates_arr_start
                                                    )
                                                );
                                                $selected_dates_arr_end_str = strtotime(
                                                    "12:00 a.m.",
                                                    strtotime(
                                                        $selected_dates_arr_end
                                                    )
                                                );

                                                if (
                                                    $selected_dates_arr_start_str <
                                                    $enabling_date_from_str ||
                                                    $selected_dates_arr_end_str >
                                                    $enabling_date_to_str
                                                ) {
                                                    $valid = false;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        break;

                        case "multiple_dates":
                        $enabling_specific_dates_arr = array_map(
                            "trim",
                            explode(",", $enabling_specific_dates)
                        );
                        $selected_dates_arr = array_map(
                            "trim",
                            explode(",", $selected_dates)
                        );
                        if (isset($selected_dates_arr[0])) {
                            $selected_dates_arr_date_only = [];

                            foreach (
                                $selected_dates_arr
                                as $sdkey => $sdvalue
                            ) {
                                $date_only = explode(" ", $sdvalue);
                                if (isset($date_only[0])) {
                                    $selected_dates_arr_date_only[] =
                                    $date_only[0];
                                }
                            }

                                // check if selected date belogs to the enabled dates
                            if (
                                array_intersect(
                                    $selected_dates_arr_date_only,
                                    $enabling_specific_dates_arr
                                ) != $selected_dates_arr_date_only
                            ) {
                                $valid = false;
                            }

                            if (!$valid) {
                                $valid = true;

                                    // check if selected date belogs to the enabled dates
                                if ($enabling_ranges_date != "") {
                                    $enabling_ranges_date_arr = array_map(
                                        "trim",
                                        explode(",", $enabling_ranges_date)
                                    );

                                    if (
                                        is_array($enabling_ranges_date_arr)
                                    ) {
                                        foreach (
                                            $enabling_ranges_date_arr
                                            as $disabled_range_key =>
                                            $disabled_range_value
                                        ) {
                                            $enabling_ranges_date_from_to = array_map(
                                                "trim",
                                                explode(
                                                    "to",
                                                    $disabled_range_value
                                                )
                                            );

                                            if (
                                                is_array(
                                                    $enabling_ranges_date_from_to
                                                ) &&
                                                isset(
                                                    $enabling_ranges_date_from_to[1]
                                                )
                                            ) {
                                                $enabling_date_from =
                                                $enabling_ranges_date_from_to[0];
                                                $enabling_date_to =
                                                $enabling_ranges_date_from_to[1];

                                                foreach (
                                                    $selected_dates_arr
                                                    as $skey => $svalue
                                                ) {
                                                    $flatpickr_is_date_bwtween = flatpickr_is_date_bwtween(
                                                        $svalue,
                                                        $enabling_date_from,
                                                        $enabling_date_to
                                                    );
                                                    if (
                                                        !$flatpickr_is_date_bwtween
                                                    ) {
                                                        $valid = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        break;
                    }
                } else {
                    $valid = true;

                    switch ($datepicker_type) {
                        case "single_datetime":
                        $selected_dates_arr = array_map(
                            "trim",
                            explode(",", $selected_dates)
                        );
                        if (isset($selected_dates_arr[0])) {
                            $selected_date = $selected_dates_arr[0];

                                // check if selected date belogs to the disabled dates
                            if ($disabling_specific_dates != "") {
                                $disabling_specific_dates_arr = array_map(
                                    "trim",
                                    explode(",", $disabling_specific_dates)
                                );

                                $selected_date_only_arr = explode(
                                    " ",
                                    $selected_date
                                );
                                $selected_date_only =
                                $selected_date_only_arr[0];

                                $disabling_specific_dates_arr = reformat_dates(
                                    $disabling_specific_dates_arr
                                );
                                $selected_date_only = reformat_date(
                                    $selected_date_only
                                );
                                if (
                                    in_array(
                                        $selected_date_only,
                                        $disabling_specific_dates_arr
                                    )
                                ) {
                                    $valid = false;
                                }
                            }

                            if ($valid) {
                                $valid = true;

                                    // check if selected date belogs to the disabled dates
                                if ($disabling_ranges_date != "") {
                                    $disabling_ranges_date_arr = array_map(
                                        "trim",
                                        explode(",", $disabling_ranges_date)
                                    );

                                    if (
                                        is_array($disabling_ranges_date_arr)
                                    ) {
                                        foreach (
                                            $disabling_ranges_date_arr
                                            as $disabled_range_key =>
                                            $disabled_range_value
                                        ) {
                                            $disabling_ranges_date_from_to = array_map(
                                                "trim",
                                                explode(
                                                    "to",
                                                    $disabled_range_value
                                                )
                                            );

                                            if (
                                                is_array(
                                                    $disabling_ranges_date_from_to
                                                ) &&
                                                isset(
                                                    $disabling_ranges_date_from_to[1]
                                                )
                                            ) {
                                                $disabling_date_from =
                                                $disabling_ranges_date_from_to[0];
                                                $disabling_date_to =
                                                $disabling_ranges_date_from_to[1];
                                                $flatpickr_is_date_bwtween = flatpickr_is_date_bwtween(
                                                    $selected_date,
                                                    $disabling_date_from,
                                                    $disabling_date_to
                                                );

                                                if (
                                                    $flatpickr_is_date_bwtween
                                                ) {
                                                    $valid = false;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if ($valid) {
                                $is_greater_min_date = flatpickr_comapre_dates(
                                    $selected_date,
                                    $min_date,
                                    $operation = "greater"
                                );
                                $is_lessthan_max_date = flatpickr_comapre_dates(
                                    $selected_date,
                                    $max_date,
                                    $operation = "lessthan"
                                );
                                if (
                                    !$is_greater_min_date ||
                                    !$is_lessthan_max_date
                                ) {
                                    $valid = false;
                                }
                            }
                        }

                        break;

                        case "date_range":
                        $valid = true;
                        $selected_dates_arr = array_map(
                            "trim",
                            explode("to", $selected_dates)
                        );

                        if (isset($selected_dates_arr[1])) {
                            $selected_dates_arr_start =
                            $selected_dates_arr[0];
                            $selected_dates_arr_end =
                            $selected_dates_arr[1];

                                // check if there are specific disabled dates
                            if ($disabling_specific_dates != "") {
                                $disabling_specific_dates_arr = array_map(
                                    "trim",
                                    explode(",", $disabling_specific_dates)
                                );

                                $selected_dates_arr_start_donly = explode(
                                    " ",
                                    $selected_dates_arr_start
                                );
                                $selected_dates_start_donly =
                                $selected_dates_arr_start_donly[0];
                                $selected_dates_arr_end_donly = explode(
                                    " ",
                                    $selected_dates_arr_end
                                );
                                $selected_dates_end_donly =
                                $selected_dates_arr_end_donly[0];

                                    // check if selected date belogs to the disabled dates
                                foreach (
                                    $disabling_specific_dates_arr
                                    as $dkey => $dvalue
                                ) {
                                    $flatpickr_is_date_bwtween = flatpickr_is_date_bwtween(
                                        $dvalue,
                                        $selected_dates_start_donly,
                                        $selected_dates_end_donly
                                    );
                                    if ($flatpickr_is_date_bwtween) {
                                        $valid = false;
                                        break;
                                    }
                                }
                            }
                            if ($valid) {
                                $valid = true;

                                    // check if selected date belogs to the disabled dates
                                if ($disabling_ranges_date != "") {
                                    $disabling_ranges_date_arr = array_map(
                                        "trim",
                                        explode(",", $disabling_ranges_date)
                                    );
                                    if (
                                        is_array($disabling_ranges_date_arr)
                                    ) {
                                        foreach (
                                            $disabling_ranges_date_arr
                                            as $disabled_range_key =>
                                            $disabled_range_value
                                        ) {
                                            $disabling_ranges_date_from_to = array_map(
                                                "trim",
                                                explode(
                                                    "to",
                                                    $disabled_range_value
                                                )
                                            );
                                            if (
                                                is_array(
                                                    $disabling_ranges_date_from_to
                                                ) &&
                                                isset(
                                                    $disabling_ranges_date_from_to[1]
                                                )
                                            ) {
                                                $disabling_date_from =
                                                $disabling_ranges_date_from_to[0];
                                                $disabling_date_to =
                                                $disabling_ranges_date_from_to[1];
                                                $disabling_date_from_str = strtotime(
                                                    $disabling_date_from
                                                );
                                                $disabling_date_to_str = strtotime(
                                                    $disabling_date_to
                                                );
                                                $selected_dates_arr_start_str = strtotime(
                                                    "12:00 a.m.",
                                                    strtotime(
                                                        $selected_dates_arr_start
                                                    )
                                                );
                                                $selected_dates_arr_end_str = strtotime(
                                                    "12:00 a.m.",
                                                    strtotime(
                                                        $selected_dates_arr_end
                                                    )
                                                );

                                                $flatpickr_is_date_bwtween = flatpickr_is_date_bwtween(
                                                    $selected_dates_arr_start,
                                                    $disabling_date_from,
                                                    $disabling_date_to
                                                );
                                                $flatpickr_is_date_bwtween1 = flatpickr_is_date_bwtween(
                                                    $selected_dates_arr_end,
                                                    $disabling_date_from,
                                                    $disabling_date_to
                                                );

                                                if (
                                                    $flatpickr_is_date_bwtween ||
                                                    $flatpickr_is_date_bwtween1
                                                ) {
                                                    $valid = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if ($valid) {
                                $is_greater_min_date = flatpickr_comapre_dates(
                                    $selected_dates_arr_start,
                                    $min_date,
                                    $operation = "greater"
                                );
                                $is_lessthan_max_date = flatpickr_comapre_dates(
                                    $selected_dates_arr_end,
                                    $max_date,
                                    $operation = "lessthan"
                                );
                                if (
                                    !$is_greater_min_date ||
                                    !$is_lessthan_max_date
                                ) {
                                    $valid = false;
                                }
                            }
                        }

                        break;

                        case "multiple_dates":
                        $disabling_specific_dates_arr = array_map(
                            "trim",
                            explode(",", $disabling_specific_dates)
                        );
                        $selected_dates_arr = array_map(
                            "trim",
                            explode(",", $selected_dates)
                        );
                        if (isset($selected_dates_arr[0])) {
                            $selected_dates_arr_date_only = [];

                            foreach (
                                $selected_dates_arr
                                as $sdkey => $sdvalue
                            ) {
                                $date_only = explode(" ", $sdvalue);
                                if (isset($date_only[0])) {
                                    $selected_dates_arr_date_only[] =
                                    $date_only[0];
                                }
                            }

                                // check if selected date belogs to the disabled dates
                            $array_intersect = array_intersect(
                                $selected_dates_arr_date_only,
                                $disabling_specific_dates_arr
                            );

                            if (count($array_intersect) > 0) {
                                $valid = false;
                            }

                            if ($valid) {
                                $valid = true;

                                    // check if selected date belogs to the disabled dates
                                if ($disabling_ranges_date != "") {
                                    $disabling_ranges_date_arr = array_map(
                                        "trim",
                                        explode(",", $disabling_ranges_date)
                                    );

                                    if (
                                        is_array($disabling_ranges_date_arr)
                                    ) {
                                        foreach (
                                            $disabling_ranges_date_arr
                                            as $disabled_range_key =>
                                            $disabled_range_value
                                        ) {
                                            $disabling_ranges_date_from_to = array_map(
                                                "trim",
                                                explode(
                                                    "to",
                                                    $disabled_range_value
                                                )
                                            );

                                            if (
                                                is_array(
                                                    $disabling_ranges_date_from_to
                                                ) &&
                                                isset(
                                                    $disabling_ranges_date_from_to[1]
                                                )
                                            ) {
                                                $disabling_date_from =
                                                $disabling_ranges_date_from_to[0];
                                                $disabling_date_to =
                                                $disabling_ranges_date_from_to[1];

                                                foreach (
                                                    $selected_dates_arr
                                                    as $skey => $svalue
                                                ) {
                                                    $flatpickr_is_date_bwtween = flatpickr_is_date_bwtween(
                                                        $svalue,
                                                        $disabling_date_from,
                                                        $disabling_date_to
                                                    );
                                                    if (
                                                        $flatpickr_is_date_bwtween
                                                    ) {
                                                        $valid = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if ($valid) {
                                foreach (
                                    $selected_dates_arr
                                    as $key => $selected_date_value
                                ) {
                                    $is_greater_min_date = flatpickr_comapre_dates(
                                        $selected_date_value,
                                        $min_date,
                                        $operation = "greater"
                                    );
                                    $is_lessthan_max_date = flatpickr_comapre_dates(
                                        $selected_date_value,
                                        $max_date,
                                        $operation = "lessthan"
                                    );
                                    if (
                                        !$is_greater_min_date ||
                                        !$is_lessthan_max_date
                                    ) {
                                        $valid = false;
                                        break;
                                    }
                                }
                            }
                        }

                        break;
                    }
                }
                $error_message = "Please enter valid date(s).";
                if (!$valid) {
                    $form_state->setError($element, t($error_message));
                }
            }
        }
    }
}
