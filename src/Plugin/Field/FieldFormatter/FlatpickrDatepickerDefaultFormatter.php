<?php

namespace Drupal\flatpickr_datepicker\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'flatpickr_datepicker' formatter.
 *
 * @FieldFormatter(
 *   id = "flatpickr_datepicker_default_formatter",
 *   module = "flatpickr_datepicker",
 *   label = @Translation("Flatpickr Datepicker "),
 *   field_types = {
 *     "flatpickr_datepicker"
 *   }
 * )
 */
class FlatpickrDatepickerDefaultFormatter extends FormatterBase {

 /**
   * {@inheritdoc}
   */
 public static function defaultSettings() {
    return [
      'display_layout' => 'calendar_view',
      'date_format' => 'F j, Y',
  ] + parent::defaultSettings();
}

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['display_layout'] = [
        '#type' => 'select',
        '#title' => $this->t('Months display layout'),
        '#description' => $this->t('Select the layout of the months to be displayed'),
        '#options' => [
            'comma_separated' => $this->t('Comma separated'),
            'list_view' => $this->t('List view'),
            'calendar_view' => $this->t('Calendar view')
        ],
        '#default_value' => $this->getSetting('display_layout')??'calendar_view',
        '#required' => false
    ];

    $element['date_format'] = [
        '#type' => 'select',
        '#title' => $this->t('Date format'),
        '#description' => $this->t('Select the format of the date to be displayed'),
        '#options' => [
            'F j, Y' => $this->t(date('F j, Y')),
            'jS F, Y' => $this->t(date('jS F, Y')),
            'm.d.y' => $this->t(date('m.d.y')),
            'Ymd' => $this->t(date('Ymd')),
            'D M j, Y' => $this->t(date('D M j, Y')),
            'Y-m-d' => $this->t(date('Y-m-d')),
            'd-m-Y' => $this->t(date('d-m-Y')),
            'j-M-Y' => $this->t(date('j-M-Y')),
            'F j' => $this->t(date('F j')),
            'jS F' => $this->t(date('jS F')),
            'F j, Y H:i A' => $this->t(date('F j, Y H:i A')),
            'jS F, Y H:i A' => $this->t(date('jS F, Y H:i A')),
            'm.d.y H:i A' => $this->t(date('m.d.y H:i A')),
            'Ymd H:i A' => $this->t(date('Ymd H:i A')),
            'D M j, Y H:i A' => $this->t(date('D M j, Y H:i A')),
            'Y-m-d H:i A' => $this->t(date('Y-m-d H:i A')),
            'd-m-Y H:i A' => $this->t(date('d-m-Y H:i A')),
            'j-M-Y H:i A' => $this->t(date('j-M-Y H:i A')),
            'F j H:i A' => $this->t(date('F j H:i A')),
            'jS F H:i A' => $this->t(date('jS F H:i A')),
        ],
        '#default_value' => $this->getSetting('date_format')??'F j, Y',
        '#required' => false,
        '#states' => [
            'visible' => [
                ':input[name$="[settings_edit_form][settings][display_layout]"]' => ['!value' => 'calendar_view'],
            ],
        ]
    ];

    return $element;
}

    /**
     * {@inheritdoc}
     */
    public function settingsSummary() {
        $display_layout = $this->getSetting('display_layout');
        $date_format = $this->getSetting('date_format');

        $summary_content = 'Display layout: '.$display_layout.'<br>';


        if($display_layout != 'calendar_view') {
            $summary_content .= 'The the date format: '.$date_format;
        }

       // Implement settings summary.
        $summary[] = $this->t($summary_content);

        return $summary;
    }

     /**
      * Builds a renderable array for a field value.
      *
      * @param \Drupal\Core\Field\FieldItemListInterface $items
      *   The field values to be rendered.
      * @param string $langcode
      *   The language that should be used to render the field.
      *
      * @return array
      *   A renderable array for $items, as an array of child elements keyed by
      *   consecutive numeric indexes starting from 0.
      */
     public function viewElements(FieldItemListInterface $items, $langcode)
     {
        $elements = array();
        $display_layout = $this->getSetting('display_layout');
        $other_data['display_layout'] = $display_layout;

        $calendar_type = 'multiple';

        $date_format = $this->getSetting('date_format');
        $other_data['date_format'] = $date_format;

        foreach ($items as $delta => $item) {
            $formatted_dates = array();

            $all_dates = $item->flatpickr_datepicker;
            if($display_layout != 'calendar_view') {
                if($all_dates != '') {

                    if (strpos($all_dates, 'to') !== false) {
                        $date_range = array_map('trim', explode('to', $all_dates));
                        if(isset($date_range[1])) {
                            $calendar_type = 'range';
                            $start = flatpickr_formatted_date($date_range[0], $date_format);
                            $end = flatpickr_formatted_date($date_range[1], $date_format);

                            $formatted_dates[] = $start.' to '.$end;

                            if($display_layout == 'comma_separated') {
                                $formatted_dates = "<span class='comma-separated-date'>".implode('</span>, <span class="comma_separated-date">', $formatted_dates)."</span>";
                            }
                        }
                    } else {
                        $formatted_dates_values = array_map('trim', explode(',', $all_dates));
                        foreach ($formatted_dates_values as $key => $date_value) {
                            $formatted_dates[] = flatpickr_formatted_date($date_value, $date_format);
                        }

                        if($display_layout == 'comma_separated') {
                            $formatted_dates = "<span class='comma-separated-date'>".implode('</span>, <span class="comma_separated-date">', $formatted_dates)."</span>";
                        }
                    }
                }

                $other_data['formatted_dates'] = $formatted_dates;
            } else {
                if (strpos($all_dates, 'to') !== false) {
                    $date_range = array_map('trim', explode('to', $all_dates));
                    if(isset($date_range[1])) {
                        $calendar_type = 'range';
                        $start = flatpickr_formatted_date($date_range[0], $date_format);
                        $end = flatpickr_formatted_date($date_range[1], $date_format);

                        $formatted_dates[] = $start.' to '.$end;

                        if($display_layout == 'comma_separated') {
                            $formatted_dates = "<span class='comma-separated-date'>".implode('</span>, <span class="comma_separated-date">', $formatted_dates)."</span>";
                        }
                    }
                }
            }
            $other_data['calendar_type'] = $calendar_type;

            $elements[$delta] = array(
                '#theme' => 'flatpickr_datepicker',
                '#flatpickr_datepicker' => $item->flatpickr_datepicker,
                '#other_data' => $other_data,
            );
        }

        return $elements;
    }
}